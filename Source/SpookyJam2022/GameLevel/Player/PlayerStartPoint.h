﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "PlayerStartPoint.generated.h"

UCLASS()
class SPOOKYJAM2022_API APlayerStartPoint : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	APlayerStartPoint();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Gameplay")
	int ExpectedPlayerIndex;
};
