// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Buildings/MainBuildingActor.h"
#include "Enemies/MeleeEnemy.h"
#include "GameFramework/GameModeBase.h"
#include "Player/PlayerStartPoint.h"
#include "Player/WorldPlayer.h"
#include "Units/GathererUnit.h"
#include "Units/WarriorUnit.h"
#include "StrategyGameMode.generated.h"

/**
 * 
 */
UCLASS()
class SPOOKYJAM2022_API AStrategyGameMode : public AGameModeBase
{
	GENERATED_BODY()

	AStrategyGameMode();

	virtual void BeginPlay() override;

	virtual void RestartPlayer(AController* NewPlayer) override;

	void ReturnToMenu() const;

	/** Helper method for UI, to exit game. */
	static void ExitGame();

protected:

	UPROPERTY(EditAnywhere, Category="Gameplay")
	TSubclassOf<AMainBuildingActor> MainBuildingClass;

	UPROPERTY(EditAnywhere, Category="Gameplay")
	TSubclassOf<AGathererUnit> UnitGathererClass;

	UPROPERTY(EditAnywhere, Category="Gameplay")
	TSubclassOf<AWarriorUnit> UnitWarriorClass;

	UPROPERTY(EditAnywhere, Category="Gameplay")
	TSubclassOf<AMeleeEnemy> EnemyMeleeClass;

	UPROPERTY(EditAnywhere, Category="Gameplay")
	TSubclassOf<AWorldPlayer> WorldPlayerClass;

	UPROPERTY(EditAnywhere, Category="Gameplay")
	TSubclassOf<APlayerStartPoint> PlayerStartPointClass;
};
