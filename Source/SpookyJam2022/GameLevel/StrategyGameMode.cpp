#include "StrategyGameMode.h"

#include "StrategyPlayerController.h"
#include "Kismet/GameplayStatics.h"

AStrategyGameMode::AStrategyGameMode()
{
	PlayerControllerClass = AStrategyPlayerController::StaticClass();
	MainBuildingClass = AMainBuildingActor::StaticClass();
	UnitGathererClass = AGathererUnit::StaticClass();
	UnitWarriorClass = AWarriorUnit::StaticClass();
	EnemyMeleeClass = AMeleeEnemy::StaticClass();
	WorldPlayerClass = AWorldPlayer::StaticClass();
	PlayerStartPointClass = APlayerStartPoint::StaticClass();
}

void AStrategyGameMode::BeginPlay()
{
	Super::BeginPlay();
}

void AStrategyGameMode::RestartPlayer(AController* NewPlayer)
{
	 //Super::RestartPlayer(NewPlayer);
	AStrategyPlayerController* const NewPlayerController = Cast<AStrategyPlayerController>(NewPlayer);
	if (NewPlayerController != nullptr)
	{
		UWorld* World = GetWorld();
		FConstPlayerControllerIterator PlayerControllerIterator = World->GetPlayerControllerIterator();
		while (*PlayerControllerIterator != NewPlayerController)
		{
			++PlayerControllerIterator;
		}
		const int OurPlayerIndex = PlayerControllerIterator.GetIndex();
		// UE_LOG(LogTemp, Display, TEXT("Found our player at index %i"), OurPlayerIndex)
		const FString ExpectedPlayerTag = FString::Printf(TEXT("Player%i"), OurPlayerIndex);
		const AActor* PlayerStart = FindPlayerStart(NewPlayerController, ExpectedPlayerTag);

		const FVector ActorLocation = PlayerStart->GetActorLocation();
		const FRotator ActorRotation = PlayerStart->GetActorRotation();
		// Get the Index of this starting point, spawn the stuff here!
		AActor* WorldPlayer = World->SpawnActor(*WorldPlayerClass, &ActorLocation, &ActorRotation);
		NewPlayerController->SetViewTarget(WorldPlayer);

		World->SpawnActor(*MainBuildingClass, &ActorLocation, &ActorRotation);
		
		UE_LOG(LogTemp, Display, TEXT("SpawnedPlayer %i"), OurPlayerIndex)
	}
}

void AStrategyGameMode::ReturnToMenu() const
{
	GetWorld()->ServerTravel(FString("/Game/Lobby/GameLevel/Lobby"));
}

void AStrategyGameMode::ExitGame()
{
	if (GEngine)
	{
		/*AStrategyPlayerController* PlayerController = nullptr;
		PlayerController = Cast<AStrategyPlayerController>(GEngine->GetFirstLocalPlayerController(GetWorld()));
		PlayerController->ConsoleCommand(TEXT("quit"));*/
	}
}
