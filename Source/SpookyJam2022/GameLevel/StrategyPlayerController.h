// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "StrategyPlayerController.generated.h"

/**
 * A Player controller
 */
UCLASS()
class SPOOKYJAM2022_API AStrategyPlayerController : public APlayerController
{
	GENERATED_BODY()

	AStrategyPlayerController();
	
};
