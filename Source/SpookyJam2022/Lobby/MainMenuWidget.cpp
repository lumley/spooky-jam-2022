// Fill out your copyright notice in the Description page of Project Settings.


#include "MainMenuWidget.h"

#include "LobbyGameInstanceSubsystem.h"
#include "LobbyGameMode.h"
#include "Components/Button.h"
#include "Components/EditableText.h"
#include "Kismet/GameplayStatics.h"


void UMainMenuWidget::NativeConstruct()
{
	Super::NativeConstruct();

	ServerAddress->SetText(FText::FromString("127.0.0.1"));

	CreateGame->OnClicked.AddDynamic(this, &UMainMenuWidget::CreateGameClicked);
}

void UMainMenuWidget::CreateGameClicked()
{
	UGameInstance* GameInstance = Cast<UGameInstance>(UGameplayStatics::GetGameInstance(GetWorld()));
	GameInstance->GetSubsystem<ULobbyGameInstanceSubsystem>()->CreateSession();
}

