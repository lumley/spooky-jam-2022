// Fill out your copyright notice in the Description page of Project Settings.


#include "LobbyGameInstanceSubsystem.h"
#include "OnlineSubsystem.h"
// required for FOnlineSessionSettings
#include "OnlineSessionSettings.h"
// required for IOnlineSessionPtr
#include "Interfaces/OnlineSessionInterface.h"
// required for IOnlineIdentityPtr
#include "Interfaces/OnlineIdentityInterface.h"


ULobbyGameInstanceSubsystem::ULobbyGameInstanceSubsystem()
{
	OnlineSubsystem = IOnlineSubsystem::Get();
	LocalServerSessionName = FName(TEXT("GameServer"));
	LocalClientSessionName = FName(TEXT("GameClient"));
	bIsLoggedIn = true;
}

void ULobbyGameInstanceSubsystem::Login(FString Username)
{
	UE_LOG(LogTemp, Warning, TEXT("ULobbyGameInstanceSubsystem::Login()"));

	if (OnlineSubsystem)
	{
		if (IOnlineIdentityPtr Identity = OnlineSubsystem->GetIdentityInterface())
		{
			Identity->OnLoginCompleteDelegates->AddUObject(this, &ULobbyGameInstanceSubsystem::OnLoginComplete);
			Identity->Login(0, FOnlineAccountCredentials());
		}
	}
}

void ULobbyGameInstanceSubsystem::OnLoginComplete(int ControllerIndex, bool bWasSuccessful, const FUniqueNetId& UserId,
                                                  const FString& ErrorString)
{
	UE_LOG(LogTemp, Warning, TEXT("OnLoginComplete: %d"), bWasSuccessful);

	if (IOnlineIdentityPtr IdentityPtr = OnlineSubsystem->GetIdentityInterface())
	{
		IdentityPtr->ClearOnLoginCompleteDelegates(ControllerIndex, this);
	}

	if (ErrorString.Contains(TEXT("already logged in")))
	{
		bIsLoggedIn = true;
		OnLoginSuccess.Broadcast();

		return;
	}

	bIsLoggedIn = bWasSuccessful;

	if (bIsLoggedIn)
	{
		OnLoginSuccess.Broadcast();
		return;
	}

	OnLoginError.Broadcast(ErrorString);
}

void ULobbyGameInstanceSubsystem::Logout()
{
	UE_LOG(LogTemp, Warning, TEXT("ULobbyGameInstanceSubsystem::Logout()"));

	if (OnlineSubsystem)
	{
		if (IOnlineIdentityPtr Identity = OnlineSubsystem->GetIdentityInterface())
		{
			Identity->OnLogoutCompleteDelegates->AddUObject(this, &ULobbyGameInstanceSubsystem::OnLogoutComplete);
			Identity->Logout(0);
		}
	}
}

void ULobbyGameInstanceSubsystem::OnLogoutComplete(int ControllerIndex, bool bWasSuccessful)
{
	UE_LOG(LogTemp, Warning, TEXT("ULobbyGameInstanceSubsystem::OnLogoutComplete(): %d"), bWasSuccessful);

	bIsLoggedIn = !bWasSuccessful;

	if (IOnlineIdentityPtr IdentityPtr = OnlineSubsystem->GetIdentityInterface())
	{
		IdentityPtr->ClearOnLogoutCompleteDelegates(ControllerIndex, this);
	}

	if (!bWasSuccessful)
	{
		OnLogoutError.Broadcast();
	}
	else
	{
		OnLogoutSuccess.Broadcast();
	}
}

void ULobbyGameInstanceSubsystem::CreateSession()
{
	UE_LOG(LogTemp, Warning, TEXT("ULobbyGameInstanceSubsystem::CreateSession()"));

	if (!bIsLoggedIn)
	{
		UE_LOG(LogTemp, Warning, TEXT("User not logged in."));

		return;
	}

	if (OnlineSubsystem)
	{
		if (IOnlineSessionPtr SessionPtr = OnlineSubsystem->GetSessionInterface())
		{
			FOnlineSessionSettings SessionSettings;
			SessionSettings.bIsDedicated = false;
			SessionSettings.bShouldAdvertise = true;
			SessionSettings.bIsLANMatch = true;
			SessionSettings.NumPublicConnections = 5;
			SessionSettings.bAllowJoinInProgress = true;
			SessionSettings.bAllowJoinViaPresence = true;
			SessionSettings.bUsesPresence = true;
			SessionPtr->OnCreateSessionCompleteDelegates.AddUObject(
				this, &ULobbyGameInstanceSubsystem::OnSessionCreated);
			SessionPtr->CreateSession(0, LocalServerSessionName, SessionSettings);
		}
	}
}

void ULobbyGameInstanceSubsystem::OnSessionCreated(FName SessionName, bool bWasSuccess)
{
	UE_LOG(LogTemp, Warning, TEXT("ULobbyGameInstanceSubsystem::OnSessionCreated: %d"), bWasSuccess);

	if (IOnlineSessionPtr SessionPtr = OnlineSubsystem->GetSessionInterface())
	{
		SessionPtr->ClearOnCreateSessionCompleteDelegates(this);
	}

	GetWorld()->ServerTravel(FString(TEXT("GameMap?listen")));
}

void ULobbyGameInstanceSubsystem::DestroySession(bool bShutdown)
{
	UE_LOG(LogTemp, Warning, TEXT("ULobbyGameInstanceSubsystem::DestroySession()"));

	if (!bIsLoggedIn)
	{
		UE_LOG(LogTemp, Warning, TEXT("User not logged in."));

		return;
	}

	bShouldShutdown = bShutdown;

	if (OnlineSubsystem)
	{
		if (IOnlineSessionPtr SessionPtr = OnlineSubsystem->GetSessionInterface())
		{
			SessionPtr->OnDestroySessionCompleteDelegates.AddUObject(
				this, &ULobbyGameInstanceSubsystem::OnSessionDestroyed);

			if (GetWorld()->GetNetMode() == ENetMode::NM_ListenServer)
			{
				SessionPtr->DestroySession(LocalServerSessionName);
			}
			else
			{
				SessionPtr->DestroySession(LocalClientSessionName);
			}
		}
	}
}

void ULobbyGameInstanceSubsystem::OnSessionDestroyed(FName SessionName, bool bWasSuccess)
{
	UE_LOG(LogTemp, Warning, TEXT("ULobbyGameInstanceSubsystem::OnSessionDestroyed: %d"), bWasSuccess);

	if (IOnlineSessionPtr SessionPtr = OnlineSubsystem->GetSessionInterface())
	{
		SessionPtr->ClearOnDestroySessionCompleteDelegates(this);
	}

	if (bShouldShutdown)
	{
		UE_LOG(LogTemp, Warning, TEXT("ULobbyGameInstanceSubsystem::OnSessionDestroyed: Calling Shutdown"));

		// Super::Shutdown();
	}
}

void ULobbyGameInstanceSubsystem::FindSessions()
{
	UE_LOG(LogTemp, Warning, TEXT("ULobbyGameInstanceSubsystem::SearchForSessions()"));

	if (!bIsLoggedIn)
	{
		UE_LOG(LogTemp, Warning, TEXT("User not logged in."));

		return;
	}

	if (OnlineSubsystem)
	{
		if (IOnlineSessionPtr SessionPtr = OnlineSubsystem->GetSessionInterface())
		{
			TSharedPtr<FOnlineSessionSearch> SearchSettings;

			SessionSearch = MakeShareable(new FOnlineSessionSearch());

			if (SessionSearch.IsValid())
			{
				SessionSearch->MaxSearchResults = 100;
				SessionPtr->OnFindSessionsCompleteDelegates.AddUObject(
					this, &ULobbyGameInstanceSubsystem::OnFindSessionsComplete);
				SessionPtr->FindSessions(0, SessionSearch.ToSharedRef());
			}
		}
	}
}

void ULobbyGameInstanceSubsystem::OnFindSessionsComplete(bool bWasSuccess)
{
	UE_LOG(LogTemp, Warning, TEXT("ULobbyGameInstanceSubsystem::OnFindSessionsComplete: %d"), bWasSuccess);

	if (IOnlineSessionPtr SessionPtr = OnlineSubsystem->GetSessionInterface())
	{
		SessionPtr->ClearOnFindSessionsCompleteDelegates(this);
	}

	if (bWasSuccess && SessionSearch.IsValid())
	{
		TArray<FOnlineSessionWrapper> Results;

		if (SessionSearch->SearchResults.Num() > 0)
		{
			if (IOnlineSessionPtr SessionPtr = OnlineSubsystem->GetSessionInterface())
			{
				for (const FOnlineSessionSearchResult& SearchResult : SessionSearch->SearchResults)
				{
					FOnlineSessionWrapper Result;
					Result.PingInMs = SearchResult.PingInMs;
					Result.OwnerUsername = SearchResult.Session.OwningUserName;
					Result.OnlineResult = SearchResult;
					Result.SessionId = SearchResult.Session.GetSessionIdStr();

					UE_LOG(LogTemp, Warning,
					       TEXT("ULobbyGameInstanceSubsystem::OnFindSessionsComplete: SessionInfo %s"),
					       *SearchResult.Session.SessionInfo->ToDebugString());

					SessionPtr->GetResolvedConnectString(SearchResult, NAME_GamePort, Result.ConnectionString);

					Results.Add(Result);
				}
			}
		}

		OnSessionsFound.Broadcast(Results);

		return;
	}

	OnSessionsFoundError.Broadcast();
}

void ULobbyGameInstanceSubsystem::JoinOnlineSession(FOnlineSessionWrapper Result)
{
	UE_LOG(LogTemp, Warning, TEXT("ULobbyGameInstanceSubsystem::JoinSession()"));

	if (!bIsLoggedIn)
	{
		UE_LOG(LogTemp, Warning, TEXT("User not logged in."));

		return;
	}

	if (OnlineSubsystem)
	{
		if (IOnlineSessionPtr SessionPtr = OnlineSubsystem->GetSessionInterface())
		{
			SessionPtr->OnJoinSessionCompleteDelegates.AddUObject(
				this, &ULobbyGameInstanceSubsystem::OnJoinSessionComplete);
			SessionPtr->JoinSession(0, "Test", Result.OnlineResult);

			GetGameInstance()
				->GetFirstLocalPlayerController()
				->ClientTravel(Result.ConnectionString, ETravelType::TRAVEL_Absolute);
		}
	}
}

void ULobbyGameInstanceSubsystem::OnJoinSessionComplete(FName SessionName, EOnJoinSessionCompleteResult::Type Result)
{
	UE_LOG(LogTemp, Warning, TEXT("ULobbyGameInstanceSubsystem::OnJoinSessionComplete"));

	if (IOnlineSessionPtr SessionPtr = OnlineSubsystem->GetSessionInterface())
	{
		SessionPtr->ClearOnJoinSessionCompleteDelegates(this);
	}
}
