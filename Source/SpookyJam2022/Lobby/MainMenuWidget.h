// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Components/TextBlock.h"
#include "MainMenuWidget.generated.h"

/**
 * 
 */
UCLASS()
class SPOOKYJAM2022_API UMainMenuWidget : public UUserWidget
{
	GENERATED_BODY()
public:
	UFUNCTION()
	void CreateGameClicked();
	virtual void NativeConstruct() override;
protected:
	UPROPERTY(meta=(BindWidget))
	class UEditableText* ServerAddress;

	UPROPERTY(meta=(BindWidget))
	class UButton* CreateGame;

	UPROPERTY(meta=(BindWidget))
	class UButton* JoinGame;
};
