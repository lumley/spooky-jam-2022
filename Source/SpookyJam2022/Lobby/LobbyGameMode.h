// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "MainMenuWidget.h"
#include "GameFramework/GameMode.h"
#include "LobbyGameMode.generated.h"

/**
 * 
 */
UCLASS()
class SPOOKYJAM2022_API ALobbyGameMode : public AGameMode
{
	GENERATED_BODY()
	
public:
	ALobbyGameMode();

protected:
	UPROPERTY(EditAnywhere, Category="Widgets")
	TSubclassOf<UMainMenuWidget> MainMenuClass;

	virtual void BeginPlay() override;
};
