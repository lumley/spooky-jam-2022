// Fill out your copyright notice in the Description page of Project Settings.


#include "LobbyGameMode.h"

#include "LobbyPlayerController.h"
#include "Blueprint/UserWidget.h"



ALobbyGameMode::ALobbyGameMode()
{
	PlayerControllerClass = ALobbyPlayerController::StaticClass();
}

void ALobbyGameMode::BeginPlay()
{
	Super::BeginPlay();

	if (IsValid(MainMenuClass) == false)
	{
		UE_LOG(LogTemp, Error, TEXT("Invalid Main Menu class"));
		return;
	}

	UUserWidget* UserWidget = CreateWidget(GetWorld(), MainMenuClass);
	UMainMenuWidget* MainMenuWidget = Cast<UMainMenuWidget>(UserWidget);

	if (MainMenuWidget == nullptr)
	{
		UE_LOG(LogTemp, Error, TEXT("could not create main menu widget"));
		return;
	}
	// MainMenuWidget->SetOwningPlayer(GetWorld()->GetFirstPlayerController());
	MainMenuWidget->AddToViewport();
	// GEngine->GetFirstLocalPlayerController(GetWorld())->SetInputMode(FInputModeUIOnly());
}
